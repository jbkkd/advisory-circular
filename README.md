# Advisory Circular

Advisory Circular is a twitter bot that tweets when it detects
aircraft flying in circles.

The bot uses data from the [ADS-B
Exchange](https://adsbexchange.com/), which is a network of amateurs
who track aircraft, to monitor all the aircraft flying in a particular
geographic region. When it detects a
[helicopter](https://twitter.com/SkyCirclesLA/status/1224376709048193024),
[fixed wing
aircraft](https://twitter.com/SkyCirclesDC/status/1223995896536240133),
[blimp](https://twitter.com/SkyCirclesLA/status/1213310909302493184),
or
[autogyro](https://twitter.com/SkyCirclesLA/status/1216464781869121536)
flying in circles, it will tweet a short description of the aircraft
and an image of the aircraft's track on a map.

![Example tweet](Screen Shot 2019-12-13.png?raw=true "Example tweet")

The text of the tweet includes the aircraft's registration (if known,
otherwise just its ICAO code), type, speed, altitude, and transponder
squawk code. It will also include the city over which the aircraft is
circling, and a nearby landmark—a park, school, restaurant, or some
other notable place.

The bot takes a screenshot of the aircraft as it currently appears on
the ADS-B Exchange's "Global Radar View" web page.

This code currently powers [13
bots](https://twitter.com/i/lists/1263724487435890688), including the
following bots:

  * [@SkyCirclesLA](https://twitter.com/SkyCirclesLA), which covers
    the Los Angeles metro area.
  * [@SkyCirclesSF](https://twitter.com/SkyCirclesSF), the San
    Francisco Bay area.
  * [@SkyCirclesDC](https://twitter.com/SkyCirclesDC), for the
    Washington D.C. area.
  * [@SkyCirclesR2508](https://twitter.com/SkyCirclesR2508), for the
    R-2508 airspace complex over Edwards Air Force Base.
  * [@SkyCirclesNTTR](https://twitter.com/SkyCirclesNTTR), for the
    Nevada Test and Training Range (including Groom Lake/ Area 51).


## Technologies used

This bot is written in Clojurescript, targeting Node.

The bot uses the [ADS-B Exchange
API](https://www.adsbexchange.com/data/) to get aircraft data in real
time.

It uses the [Pelias](https://pelias.io/) front-end to OpenStreetMap
and WhosOnFirst data to come up with some color description of the
aircraft's location.

It uses an embedded Chromium browser to take a screenshot of the
aircraft on the ADS-B Exchange "global Radar View".

## Criteria for tweeting

The bot will consider an aircraft to be "circling" if:

1. In the past 25 minutes, the sum of the aircraft's bearing changes
   (while in the air; not on the ground) is more than 1440 degrees.
   Port turns cancel out starboard turns and vice versa--there needs
   to be a net change of at least 1440 degrees in one direction.

2. The centroid of the last 3 minutes worth of positions must be more
   than 2.5 km away from all known airports.

3. The centroid must be located within the county of Los Angeles.
   (This is just because I don't yet have airport data outside Los
   Angeles County.)

4. The aircraft must, at the moment of decision, be at an altitude of
   300 feet or more.

5. Once an aircraft is considered to be circling, it will be tweeted
   about. It won't be eligible to be considered circling/tweeted about
   again until it stops circling and 30 minutes pass.

Many of the specific numbers in the above criteria can be customized
on a per-bot basis.

There are a lot of weird flight paths that can meet criterion #1, and
I kind of like it that way. For example, see
https://twitter.com/lemonodor/status/1203033917483175936 (consider
whether we also want to detect something like
https://twitter.com/lemonodor/status/1204550831103590400).


## Prerequisites to running the bot

**Set up a Pelias instance containing information for the geographical
area you're interested in.** Pelias is the geographic database that
the bot uses to know which city aircraft are circling over and find
nearby landmarks. It's also used to filter out aircraft that are
circling over airports. See https://github.com/pelias/docker and if
you're lucky the `projects` folder will have a predefined project that
covers the area you want. I run one using the `north-america` project.

**Get an API key for adsbexchange.com.** See
https://www.adsbexchange.com/data/. If you have a receiver and feed
them data, they'll give you a key for free. Alternately, find another
data source of realtime (or not) aircraft data and massage it into a
form that the bot can work with.

**Create a twitter account and get app access keys for it.** Follow the [instructions at
botwiki](https://botwiki.org/tutorials/how-to-create-a-twitter-app/).

**Get a `basestation.sqb` file.** `basestation.sqb` is a crowdsourced
sqlite database file that contains registration and type information
on aircraft. There is no canonical or official version of this file,
unfortunately. Honestly, it's a real mess that somehow many people in
the planetracking hobby are convinced is a good thing, or at least not
a problem. In any case, you can try downloading a version from
http://www.kinetic.co.uk/resourcesdownloads.php#4.


## Running the bot

Put your ADS-B Exchange API key and twitter keys in `secrets.yaml`.

Create a `config.yaml` that looks like this:
```yaml
adsbx:
  url: https://adsbexchange.com/api/aircraft/json
  api-key: my-api-key
pelias:
  url: http://<my pelias host>:4000/v1
lat: 34.1335
lon: -118.1924
radius-km: 10
basestation-sqb: clean-basestation.sqb
```

Make sure you have at least Node version 10.0.0 installed, and then
install the node dependencies:

`npm install`

Compile the clojurescript:

`npx shadow-cljs compile script`

Run the bot:

```
while :; do python intervalexec.py 10 node out/advisorycircular.js; sleep 5; done
```

`intervalexec.py` is a script that will try to run the bot script no
more than once every 10 seconds.


* **Do not run the script with a large radius, or adsbexchange.com will
  ban your IP.**

* **Do not run the script more often than once every 15 seconds, or
  adsbexchange.com will ban your IP.**


## Development

To run a test watcher:

```
npx shadow-cljs watch script test
```
